
public class Fahrkartenautomattest2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	import java.util.Scanner;

	import javax.swing.plaf.synth.SynthOptionPaneUI;

	class InstableFahrkartenautomat
	{
		
	    static Scanner tastatur = new Scanner(System.in);
		
	    public static void main(String[] args) {
	    	
	       double rueckgabebetrag;
	       
	       rueckgabebetrag = fahrkartenBezahlen(fahrkartenbestellungErfassen());
	       fahrkartenAusgeben();
	       rueckgeldAusgeben(rueckgabebetrag);
	    }
	    
	    public static double fahrkartenbestellungErfassen() {
	    	
	        short ticketAnzahl;
	        double zuZahlenderBetrag;
	        
	        //Konsoleneingabe Zu zahlender Betrag 
	        System.out.print("Zu zahlender Betrag (EURO): ");
	        zuZahlenderBetrag = tastatur.nextDouble();
	        
	        //Konsoleneingabe Anzahl Tickets
	        System.out.print("Anzahl der Tickets: ");
	        ticketAnzahl = tastatur.nextShort();
	        
	        return ticketAnzahl*zuZahlenderBetrag;
	    }
	    
	    public static double fahrkartenBezahlen(double zuZahlen) {
	    	
	    	double restBetrag, eingeworfeneM�nze;
	    	
	        double eingezahlterGesamtbetrag = 0;
	        while(eingezahlterGesamtbetrag < zuZahlen)
	        {
	     	   System.out.print("Noch zu zahlen: ");
	     	   restBetrag = zuZahlen - eingezahlterGesamtbetrag;
	     	   
	     	   System.out.printf("%.2f Euro%n", restBetrag);
	     	   
	     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	     	   eingeworfeneM�nze = tastatur.nextDouble();
	     	   eingezahlterGesamtbetrag += eingeworfeneM�nze;
	        }
	    	

	    	return eingezahlterGesamtbetrag - zuZahlen;
	    	
	    }
	    
	    public static void fahrkartenAusgeben() {

	        System.out.println("\nFahrschein wird ausgegeben");
	        for (int i = 0; i < 8; i++) {
	           System.out.print("=");
	           try {
	 			  Thread.sleep(250);
	           } catch (InterruptedException e) {
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	           }
	        }
	        System.out.println("\n\n");
	    	
	    }
	    
	    public static void rueckgeldAusgeben(double restBetrag) {
	    	
	     	   System.out.printf("Der R�ckgabebetrag in H�he von ");
	     	   System.out.printf("%.2f", restBetrag);
	     	   System.out.println(" Euro wird in folgenden M�nzen ausgezahlt:");

	     	   
//	            while(restBetrag >= 2.0) // 2 EURO-M�nzen
//	            {
//	         	  System.out.println("2 EURO");
//	 	          restBetrag -= 2.0;
//	            }
//	            while(restBetrag >= 1.0) // 1 EURO-M�nzen
//	            {
//	         	  System.out.println("1 EURO");
//	 	          restBetrag -= 1.0;
//	            }
//	            while(restBetrag >= 0.5) // 50 CENT-M�nzen
//	            {
//	         	  System.out.println("50 CENT");
//	 	          restBetrag -= 0.5;
//	            }
//	            while(restBetrag >= 0.2) // 20 CENT-M�nzen
//	            {
//	         	  System.out.println("20 CENT");
//	  	          restBetrag -= 0.2;
//	            }
//	            while(restBetrag >= 0.1) // 10 CENT-M�nzen
//	            {
//	         	  System.out.println("10 CENT");
//	 	          restBetrag -= 0.1;
//	            }
//	            while(restBetrag >= 0.050)// 5 CENT-M�nzen
//	            {
//	         	  System.out.println("5 CENT");
//	  	          restBetrag -= 0.05;
//	            }
//	            while(restBetrag >= 0.02)// 2 CENT-M�nzen
//	            {
//	         	  System.out.println("2 CENT");
//	  	          restBetrag -= 0.02;
//	            }
//	            while(restBetrag >= 0.001)// 1 CENT-M�nzen
//	            {
//	         	  System.out.println("1 CENT");
//	  	          restBetrag -= 0.01;
//	            }
//	     	   better solution: iterate over different coin values
	        double[] coins = { 2.0, 1.0, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01};
//	        String[] coinsValue = {
//	            "2 Euro", "1 Euro", "50 Cent", "20 Cent", "10 Cent", "5 Cent", "2 Cent", "1 Cent" };
	        
//	        for (double coin : coins) {
//	        for (int i = 0; i < coins.length; ++i) {
//	          while (restBetrag >= coins[i] || coins[i] == 0.01 && restBetrag > 0) {
//	            
//	            restBetrag -= coins[i];
//	            System.out.printf("%7s%n", coinsValue[i]);
//	          }
//	        }
	        
	      for (double coin : coins) {
	        while (restBetrag >= coin || coin == 0.01 && restBetrag > 0) {
	         // System.out.printf("%2d %s%n", (int) (coin >= 1.0 ? coin : coin * 100), coin >= 1.0 ? "Euro" : "Cent");
	          restBetrag -= coin;
	          if (coin >= 1.0) {
	            System.out.printf("%2d Euro%n", (int) coin);
	          } else {
	            System.out.printf("%2d Cent%n", (int) (coin * 100));
	          }
	        }
	      }

	        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                           "vor Fahrtantritt entwerten zu lassen!\n"+
	                           "Wir w�nschen Ihnen eine gute Fahrt.");
	        
	        // Demo: 1x2,10� Ticket mit 2x2� M�nzen bezahlen
	        System.out.println("\nFun-Fact:\n" +
	        "Restgeldsumme beim Automaten:" + restBetrag);
	    	
	    }
	    
	}

