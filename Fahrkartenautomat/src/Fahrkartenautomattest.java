import java.util.Scanner;

class Fahrkartenautomattest
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       short ticketAnzahl;
       double newZuZahlenderBetrag;

       
       //Konsoleneingabe Zu zahlender Betrag 
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       //Konsoleneingabe Anzahl Tickets
       System.out.print("Anzahl der Tickets: ");
       ticketAnzahl = tastatur.nextShort();
       
       // Ticketanzahl mit Preis multiplizieren
       zuZahlenderBetrag = ticketAnzahl*zuZahlenderBetrag;

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.001;
       while(eingezahlterGesamtbetrag <= zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: ");
    	   newZuZahlenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
    	   System.out.printf("%.2f", newZuZahlenderBetrag);
    	   System.out.printf(" Euro\n");
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }
       
       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag >= 0.001)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von ");
    	   System.out.printf("%.2f", r�ckgabebetrag);
    	   System.out.println(" Euro wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.050)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
           while(r�ckgabebetrag >= 0.02)// 2 CENT-M�nzen
           {
        	  System.out.println("2 CENT");
 	          r�ckgabebetrag -= 0.02;
           }
           while(r�ckgabebetrag >= 0.01)// 1 CENT-M�nzen
           {
        	  System.out.println("1 CENT");
 	          r�ckgabebetrag -= 0.01;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       
       // Demo: 1x2,10� Ticket mit 2x2� M�nzen bezahlen
       System.out.println("\nFun-Fact:\n" +
       "Restgeldsumme beim Automaten:" + r�ckgabebetrag);
       
    }  
}
