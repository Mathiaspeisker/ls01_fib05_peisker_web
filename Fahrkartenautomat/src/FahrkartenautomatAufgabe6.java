import java.util.Scanner;

class FahrkartenautomatAufgabe6   // FahrkartenautomatAufgabe 6-2
{
	public static Scanner tastatur = new Scanner(System.in);   // Klasse public definiert, damit die Werte aus den Methoden �bergeben werden
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);  
      
       
// (E)Benutzereingaben lesen
       double zuZahlenderBetrag; 							
       double eingezahlterGesamtbetrag;						
       double eingeworfeneM�nze;							
       double r�ckgabebetrag;
       double richtigerBetrag;
       byte anzahlTickets;		
       
      
// (A) Ausgabe
       while(true) {
       zuZahlenderBetrag = fahrkartenbestellungErfassung();
       
       r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
       fahrkartenAusgeben(); 
       
       rueckgeldAusgeben(r�ckgabebetrag);
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
               "vor Fahrtantritt entwerten zu lassen!\n"+
               "Wir w�nschen Ihnen eine gute Fahrt.\n\n");
       }
    }
                  
 // (V) Verarbeitung - vier Methoden ausgegliedert und erstellt  
    
 //   fahrkartenbestellungErfassen
    public static double fahrkartenbestellungErfassung() {
		double Fahrkartenpreis;
    	byte anzahlTickets; 
    	System.out.print("Zu zahlender Betrag (EURO): ");
        Fahrkartenpreis = tastatur.nextDouble();
        System.out.printf(" Anzahlt der Tickets: ");
        anzahlTickets = tastatur.nextByte();
    	return anzahlTickets * Fahrkartenpreis;
	}
    
//  fahrkartenBezahlen
    public static double fahrkartenBezahlen(double zuZahlen) {
	
    	double eingezahlterGesamtbetrag = 0.001f;
    	double eingeworfeneM�nze;
    	
    	while(eingezahlterGesamtbetrag <= zuZahlen)
    		{
   	   
 	   double differenz = zuZahlen - eingezahlterGesamtbetrag; 
   	   System.out.printf("Noch zu zahlen: %.2f Euro%n", differenz);
   	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
   	   eingeworfeneM�nze = tastatur.nextDouble();
          eingezahlterGesamtbetrag += eingeworfeneM�nze;		
    		}

    	return eingezahlterGesamtbetrag - zuZahlen;
    	
    }    
    
    
// fahrkartenAusgeben   
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    
//    rueckgeldAusgeben
    
    public static double rueckgeldAusgeben(double gesamtSumme) {
    	double r�ckgabebetrag = gesamtSumme;
          if(r�ckgabebetrag >= 0)
          {
       	   //System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
       	   //System.out.println("wird in folgenden M�nzen ausgezahlt:");

       	   System.out.printf("R�ckgabebetrag in H�he von %.2f Euro%n", r�ckgabebetrag);
       	   
              while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
              {
           	  System.out.println("2 EURO");
   	          r�ckgabebetrag -= 2.0;
              }
              while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
              {
           	  System.out.println("1 EURO");
   	          r�ckgabebetrag -= 1.0;
              }
              while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
              {
           	  System.out.println("50 CENT");
   	          r�ckgabebetrag -= 0.5;
              }
              while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
              {
           	  System.out.println("20 CENT");
    	          r�ckgabebetrag -= 0.2;
              }
              while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
              {
           	  System.out.println("10 CENT");
   	          r�ckgabebetrag -= 0.1;
              }
              while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
              {
           	  System.out.println("5 CENT");
    	          r�ckgabebetrag -= 0.05;
    	          }
          }
          return r�ckgabebetrag;
    
    }
}   
