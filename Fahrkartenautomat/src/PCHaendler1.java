import java.util.Scanner;

public class PCHaendler1 {

	public static void main(String[] args) {
 		Scanner myScanner = new Scanner(System.in);

		// (E)Benutzereingaben lesen
		System.out.println("was m�chten Sie bestellen?");
		String Artikel;
		Artikel = liesString(myScanner.next());
		
		System.out.println("Geben Sie die Anzahl ein:");
		int Anzahl;
		Anzahl = liesInt(myScanner.nextInt());
		
		System.out.println("Geben Sie den Nettopreis ein:");
		double Preis;
		Preis = liesDouble(myScanner.nextDouble());
		
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double Mwst;	
		Mwst = liesDouble(myScanner.nextDouble());
		
		// (A) Ausgabe
		double n;
		double b;
		n = nettogesamtpreis(Anzahl, Preis);
		b = bruttogesamtpreis(n, Mwst);
	    
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", Artikel, Anzahl, n);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", Artikel, Anzahl, b, Mwst, "%");
		
		
	}
		// (V) Verarbeitung
		public static String liesString(String text) {
			return text;
			}
		public static int liesInt(int text) {
			return text;
			}
		public static double liesDouble(double text) {
			return text;
		}
		public static double nettogesamtpreis(double anzahl, double preis) {
			 double x = anzahl;
			 double y = preis;
			 double result = x * y;
			 return result;
		 }
	      public static double bruttogesamtpreis(double nettogesamtpreis, double mwst) {  
			return nettogesamtpreis * (1 + mwst / 100);
	      }
}
