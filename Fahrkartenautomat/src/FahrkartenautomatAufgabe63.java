import java.util.Scanner;

class FahrkartenautomatAufgabe63  // FahrkartenautomatAufgabe 6-3
{
	public static Scanner tastatur = new Scanner(System.in);   
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);  
      
       
// (E)Benutzereingaben lesen
       double zuZahlenderBetrag; 							
       double eingezahlterGesamtbetrag;						
       double eingeworfeneM�nze;							
       double r�ckgabebetrag;
       double richtigerBetrag;
      
// (A) Ausgabe
       while(true) {

       zuZahlenderBetrag = fahrkartenbestellungErfassung();
       
       r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
       fahrkartenAusgeben(); 
       
       rueckgeldAusgeben(r�ckgabebetrag);
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
               "vor Fahrtantritt entwerten zu lassen!\n"+
               "Wir w�nschen Ihnen eine gute Fahrt.\n\n");
       }
    }
                  
 // (V) Verarbeitung - vier Methoden ausgegliedert und erweitert um Ticketarten
    
    public static double fahrkartenbestellungErfassung() {
		double Fahrkartenpreis;
    	int Fahrkartenanzahl;
    	int Fahrkartenart;
    	
    	System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
    	System.out.println("   Einzelfahrschein Regeltarif AB [2,90 EUR)         (1)");
    	System.out.println("   Tageskarte Regeltarif AB [8,60 EUR]               (2)");
    	System.out.println("   Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    	System.out.println();
    	System.out.print("Ihre Wahl:");
    	
    	Fahrkartenart = tastatur.nextInt();
    	Fahrkartenpreis = 0;
    	
    	while (Fahrkartenart >= 4 || Fahrkartenart <=0) {
    		System.out.println("<<falsche Eingabe>>");
    		System.out.println("Ihre Wahl:");
    		Fahrkartenart = tastatur.nextInt();
    	}
    	
    	if (Fahrkartenart ==1) {
    		Fahrkartenpreis = 2.90;
    	}
    	
    	if (Fahrkartenart ==2) {
    		Fahrkartenpreis = 8.60;
    	}
    	
    	if (Fahrkartenart ==3) {
    		Fahrkartenpreis = 23.50;
    	}
    	
    	System.out.println("Anzahl der Tickets: ");
    	Fahrkartenanzahl = tastatur.nextInt();
    	return Fahrkartenanzahl * Fahrkartenpreis;
    }
//  fahrkartenBezahlen
    public static double fahrkartenBezahlen(double zuZahlen) {
	
    	double eingezahlterGesamtbetrag = 0.001f;
    	double eingeworfeneM�nze;
    	
    	while(eingezahlterGesamtbetrag <= zuZahlen)
    		{
   	   
 	   double differenz = zuZahlen - eingezahlterGesamtbetrag; 
   	   System.out.printf("Noch zu zahlen: %.2f Euro%n", differenz);
   	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
   	   eingeworfeneM�nze = tastatur.nextDouble();
          eingezahlterGesamtbetrag += eingeworfeneM�nze;		
    		}

    	return eingezahlterGesamtbetrag - zuZahlen;
    	
    }    
    
    
// fahrkartenAusgeben   
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    
//    rueckgeldAusgeben
    
    public static double rueckgeldAusgeben(double gesamtSumme) {
    	double r�ckgabebetrag = gesamtSumme;
          if(r�ckgabebetrag >= 0)
          {

       	   System.out.printf("R�ckgabebetrag in H�he von %.2f Euro%n", r�ckgabebetrag);
       	   
              while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
              {
           	  System.out.println("2 EURO");
   	          r�ckgabebetrag -= 2.0;
              }
              while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
              {
           	  System.out.println("1 EURO");
   	          r�ckgabebetrag -= 1.0;
              }
              while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
              {
           	  System.out.println("50 CENT");
   	          r�ckgabebetrag -= 0.5;
              }
              while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
              {
           	  System.out.println("20 CENT");
    	          r�ckgabebetrag -= 0.2;
              }
              while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
              {
           	  System.out.println("10 CENT");
   	          r�ckgabebetrag -= 0.1;
              }
              while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
              {
           	  System.out.println("5 CENT");
    	          r�ckgabebetrag -= 0.05;
    	          }
          }
          return r�ckgabebetrag;
    
    }
}   
