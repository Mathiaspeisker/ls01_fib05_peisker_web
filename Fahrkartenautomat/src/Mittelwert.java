public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 3.0;
      double y = 6.0;
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
     
      m = berechneMittelwert(x,y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }

  public static double berechneMittelwert(double x1, double x2) {
	   
	   double mittelwert;
	   mittelwert = (x1 + x2) / 2;
       return mittelwert;
   
  }
}
