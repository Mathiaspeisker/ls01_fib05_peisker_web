
public class Aufgabe2 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.printf("0!\t =\t\t = 1");
		System.out.printf("\n");
		System.out.printf("1!\t =1\t\t = 1");
		System.out.printf("\n");
		System.out.printf("2!\t =1*2\t\t = 2");
		System.out.printf("\n");
		System.out.printf("3!\t =1*2*3\t\t = 6");
		System.out.printf("\n");
		System.out.printf("4!\t =1*2*3*4\t = 24");
		System.out.printf("\n");
		System.out.printf("5!\t =1*2*3*4*5\t = 120");
		
		System.out.printf("\n", args);
		System.out.printf("\n", args);
		
		System.out.printf("%-5s=", "0!");
		System.out.printf("%19s", "=");
		System.out.printf("%5s\n", "1");
		
		System.out.printf("%-5s=", "1!");
		System.out.printf(" 1%17s", "=");
		System.out.printf("%5s\n", "1");
		
		System.out.printf("%-5s=", "2!");
		System.out.printf(" 1 * 2 %12s", "=");
		System.out.printf("%5s\n", "2");
		
		System.out.printf("%-5s=", "3!");
		System.out.printf(" 1 * 2 * 3 %8s", "=");
		System.out.printf("%5s\n", "6");
		
		System.out.printf("%-5s=", "4!");
		System.out.printf(" 1 * 2 * 3 * 4 %4s", "=");
		System.out.printf("%5s\n", "24");
		
		System.out.printf("%-5s=", "5!");
		System.out.printf(" 1 * 2 * 3 * 4 * 5%1s", "=");
		System.out.printf("%5s\n", "120");
		
		System.out.printf("\n", args);
		System.out.printf("\n", args);
		

		System.out.printf("%-5s=%19s%5s\n", "0!", "=", "1");
		System.out.printf("%-5s= 1 %16s%5s\n", "1!", "=", "1");
		System.out.printf("%-5s= 1*2 %14s%5s\n", "2!", "=", "2");
		System.out.printf("%-5s= 1*2*3 %12s%5s\n", "3!", "=", "6");
		System.out.printf("%-5s= 1*2*3*4 %10s%5s\n", "4!", "=", "24");
		System.out.printf("%-5s= 1*2*3*4*5 %8s%5s\n", "5!", "=", "120");


		String a = "1";
		String b = "1*2";
		String c = "1*2*3";
		String d = "1*2*3*4";
		String e = "1*2*3*4*5";
		
		
		System.out.printf("%-5s=%19s%5s\n", "0!", "=", "1" );
		System.out.printf("%-5s= 1%17s%5s\n", "1!", "=", "1" );
		System.out.printf("%-5s= 1*2%15s%5s\n", "2!", "=", "2" );
		System.out.printf("%-5s= 1*2*3%13s%5s\n", "3!", "=", "6" );
		System.out.printf("%-5s= 1*2*3*4%11s%5s\n", "4!", "=", "12" );
		System.out.printf("%-5s= 1*2*3*4*5%9s%5s\n", "4!", "=", "120" );

		
	}

}



